//B1: Import Thư viện express
//import { Express } from "express";
const express = require("express");

//b2 khởi tạo app express
const app = new express();

//b3: khai báo cỗng để chạy api
const port = 8000;

//cấu hình để sử dụng json
app.use(express.json());

//Khai báo các api 
app.get('/', (request, response) => {
    let today = new Date();
    let message = `Xin chào, hôm nay là ngày ${today.getDate()} tháng ${today.getMonth() + 1} năm ${today.getFullYear()}`;
    response.json({
        message
    })
})

//khai báo phương thức get
app.get('/get-method', (req, res) => {
    console.log("Get - Method");
    res.json({
        message: "GET - METHOD"
    })
})

//khai báo phương thức post

app.post('/post-method', (req, res) => {
    console.log("Post - Method");
    res.json({
        message: "POST - METHOD"
    })
})

//khai báo phương thức put
app.put('/put-method', (req, res) => {
    console.log("Put - Method");
    res.json({
        message: "PUT - METHOD"
    })
})
//khai báo phương thức delete
app.delete('/delete-method', (req, res) => {
    console.log("Delete - Method");
    res.json({
        message: "DELETE - METHOD"
    })
})

//khai báo get param
app.get(`/get-data/:para1/:para2`, (req, res) => {
    let param1 = req.params.para1;
    let param2 = req.params.param2;
    res.json({
        param1,
        param2
    })
})

//khai báo
app.get('/query-data', (req, res) => {
    let query = req.query;
    let name = query.name;
    res.json({
        query,
        name
    })
})

//khai báo request body
app.post("/update-data", (req, res) => {
    let body = req.body;
    res.json({
        body,
    })
})

//b4: star app
app.listen(port, () => {
    console.log(`app listening on port ${port}`);
})